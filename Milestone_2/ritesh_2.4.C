#include<stdio.h>
#include<conio.h>
#include<math.h>
double FV(double rate, int nperiods, double pv)
{
	double fv;
	fv = pv * pow((1 + rate), nperiods);
	return fv;
}
int main()
{
	double rate, pv, fv;
	int nperiods;
	printf("Enter PV, rate and nperiods respectively:");
	scanf("%lf %lf %d", &pv, &rate, &nperiods);
	fv = FV(rate, nperiods, pv);
	printf("The FV is %lf", fv);
	return 0;
	getch();
}
