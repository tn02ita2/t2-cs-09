#include<stdio.h>
#include<conio.h>
#include<math.h>
double PV(double rate, int nperiods, double fv)
{
	double pv;
	pv = fv / pow((1 + rate), nperiods);
	return pv;
}
int main()
{
	double rate, pv, fv;
	int nperiods;
	printf("Enter FV, rate and nperiods respectively:");
	scanf("%lf %lf %d", &fv, &rate, &nperiods);
	pv = PV(rate, nperiods, fv);
	printf("The PV is %lf", pv);
	return 0;
	getch();
}

