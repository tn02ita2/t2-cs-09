#include <stdio.h>
#include<conio.h>
void main()
{
	float inch, cm;
	clrscr();
	printf("Enter length in inches:");
	scanf("%f", &inches);
	cm = inches * 2.54;
	printf("%.2f inches = %.2f centimeters\n", inches, cm);
	getch();
  }